Dotrix Changelog
================

Version 0.1.1, released on 18.03.2016
-------------------------------------
* Fix compiling with rust 1.7

Version 0.1.0, 26.07.2015
-------------------------

This is the public first release of Dotrix. Keep in mind that it is still very much in early
development.

### Features:
* Reads `dot.toml` files from sub-directories of one central directory (default `~/dotfiles`).
* Source and destination directory can be overridden (`--src` and `--dst` switches).
* Whole process can be simulated (`--dryrun` switch).
* Files can be ignored.

### Warnings and checks:
* Missing `dotrix` table.
* Ignoring a file multiple times.
* Multiple destinations for a single symlink.
* Specifying links for ignored files.
* Unknown files.
* Existing symlinks with different destinations.
* Non-existent symlink destinations.
* Various other filesystem checks.
